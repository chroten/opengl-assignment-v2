#include "Camera.h"
#include <glm\gtx\transform.hpp>

Camera::Camera() : 
	viewDirection(0.0f, 0.0f, -1.0f),
	UP(0.0f, 1.0f, 0.0f) {
}

//void Camera::mouseUpdate(const glm::vec2& newMousePosition) {
//	glm::vec2 mouseDelta = newMousePosition - oldMousePosition;
//	if (glm::length(mouseDelta) > 50.0f)
//	{
//		oldMousePosition = newMousePosition;
//		return;
//	}
//	const float ROTATIONAL_SPEED = 0.5f;
//	strafeDirection = glm::cross(viewDirection, UP);
//	glm::mat4 rotator = glm::rotate(-mouseDelta.x * ROTATIONAL_SPEED, UP) *
//		glm::rotate(-mouseDelta.y * ROTATIONAL_SPEED, strafeDirection);
//
//	viewDirection = glm::mat3(rotator) * viewDirection;
//
//	oldMousePosition = newMousePosition;
//}

const float MOVEMENT_SPEED = 0.1f;

glm::mat4 Camera::getWorldToViewMatrix() const {
	return glm::lookAt(position, position + viewDirection, UP);
}

void Camera::moveForward() {
	glm::vec3 direction = viewDirection;
	direction.y = 0.0f;
	position += MOVEMENT_SPEED * direction;
}

void Camera::moveBackward() {
	glm::vec3 direction = viewDirection;
	direction.y = 0.0f;
	position += -MOVEMENT_SPEED * viewDirection;
}

void Camera::strafeLeft() {
	strafeDirection = glm::cross(viewDirection, UP);
	position += MOVEMENT_SPEED * strafeDirection;
}

void Camera::strafeRight() {
	strafeDirection = glm::cross(viewDirection, UP);
	position += -MOVEMENT_SPEED * strafeDirection;
}

void Camera::lookUp() {
	viewDirection -= glm::vec3(0.0, +0.1f, 0.0);
}

void Camera::lookDown() {
	viewDirection -= glm::vec3(0.0, -0.1f, 0.0);
}

void Camera::lookLeft() {
	viewDirection -= glm::vec3(-0.1f, 0.0, 0.0);
}

void Camera::lookRight() {
	viewDirection -= glm::vec3(+0.1f, 0.0, 0.0);
}


